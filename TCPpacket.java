package com.example.root.skeditor;


public class TCPpacket {
    //gettery
    public Integer getFlag() {
        return flag;
    }

    public String getMsg() {
        return msg;
    }

    public Integer getPosition() {
        return position;
    }

    public Integer getLength() {
        return length;
    }

    public String getPacket_msg() {
        return packet_msg;
    }

    //elementy pakietu - flaga, wiadomość, pozycja, długość
    private Integer flag = 0;
    private String msg = "";
    private Integer position = 0;
    private Integer length = 0;
    //wiadomość ze złączonych pól, wysyłana do serwera
    private String packet_msg = "";

    //pakiety gdzie length jest równe msg.length()
    TCPpacket(Integer flag, Integer position, String msg) {
        this.flag = flag;
        this.position = position;
        this.msg = msg;
        if(msg != null)
            this.length = msg.length();
        else this.length = 0;
        if(flag != null && flag >= 0 && flag < 10) {
            packet_msg = String.valueOf(this.flag);
            if(this.position != null && position >= 0) {
                String str_position = String.valueOf(this.position);
                if(this.position < 10)
                    str_position = "00" + str_position;
                else if(this.position < 100)
                    str_position = "0" + str_position;
                String str_length = String.valueOf(this.length);
                if(this.length < 10)
                    str_length = "00" + str_length;
                else if(this.length < 100)
                    str_length = "0" + str_length;
                packet_msg += str_position + str_length;
            }
            if(msg != null)
                packet_msg += msg;
        }
        else packet_msg = "";
    }

    //pakiety gdzie długość msg nie równa się length
    TCPpacket(Integer flag, Integer position, String msg, Integer length) {
        this.flag = flag;
        this.position = position;
        this.msg = msg;
        this.length = length;
        if(flag != null && flag >= 0 && flag < 10) {
            packet_msg = String.valueOf(this.flag);
            if(this.position != null && position >= 0) {
                String str_position = String.valueOf(this.position);
                if(this.position < 10)
                    str_position = "00" + str_position;
                else if(this.position < 100)
                    str_position = "0" + str_position;
                String str_length = String.valueOf(this.length);
                if(this.length < 10)
                    str_length = "00" + str_length;
                else if(this.length < 100)
                    str_length = "0" + str_length;
                packet_msg += str_position + str_length;
            }
            if(msg != null)
                packet_msg += msg;
        }
        else packet_msg = "";
    }

    //dekodowanie wiadomości na pakiet pozwala łatwo
    //odrzucić błędne pakiety i wykrywać ich różne rodzaje
    //np. pakiet, gdzie msg.length() = length
    //lub pakiet gdzie te wartości są różne
    public static TCPpacket decode(String rawdata) {
        if(rawdata.matches("(\\d\\d\\d\\d\\d\\d\\d)(.*)"))
        {
            Integer flag = Integer.parseInt(rawdata.substring(0, 1));
            Integer position = Integer.parseInt(rawdata.substring(1, 4));
            Integer length = Integer.parseInt(rawdata.substring(4, 7));
            String msg = rawdata.substring(7);
            if(msg.length() != length) return new TCPpacket(flag, position, msg, length);
            return new TCPpacket(flag, position, msg);
        } else
            return null;
    }
}
