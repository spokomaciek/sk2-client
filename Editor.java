package com.example.root.skeditor;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class Editor extends AppCompatActivity {

    //blokada edycji przy wiadomościach przychodzących
    private boolean fill_lock = false;
    //kontrolka i jej listener
    EditText textarea;
    KeyListener mKeyListener;
    //nazwa pliku
    private String filename;
    //zadanie asynchroniczne
    private AsyncTask atask;

    //klient TCP i jego zadanie asynchroniczne
    TCPclient mTcpClient;
    public class ConnectTask extends AsyncTask<String, String, TCPclient> {

        @Override
        protected TCPclient doInBackground(String... message) {

            mTcpClient = new TCPclient(new TCPclient.OnMessageReceived() {
                @Override
                public void messageReceived(String message) {
                    publishProgress(message);
                }
            });
            mTcpClient.run();
            return null;
        }

        //uchwyt do wątków
        private Handler handle = new Handler();

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            //dekodujemy wiadomość do pakietu
            TCPpacket packet = TCPpacket.decode(values[0]);

            if (packet != null) {
                //na podstawie flagi decydujemy o dalszej pracy
                switch (packet.getFlag()) {
                    case 0:
                        //przy fladze 0, możemy pozwolić na pisanie i odblokować widok
                        if(!fetch_lock) {
                            unlock();
                            fetch_lock = true;
                        }
                        //po czasie, w którym kontrolka powinna już wprowadzić
                        //zmiany, zezwalamy na dalsze pisanie
                        handle.post(new Runnable() {
                            @Override
                            public void run() {
                                if(!textarea.isDirty())
                                    fill_lock = false;
                                else
                                    handle.postDelayed(this, 5);
                            }
                        });
                        break;
                    case 1:
                        //jeśli to pakiet typu delete, sprawdzamy czy posiada odpowiednią długość,
                        //co oznacza, że jest to pełen pakiet, i wykonujemt usunięcie fragmentu tekstu
                        if(packet.getPacket_msg().length() < 7) return;
                        try {
                            fill_lock = true;
                            textarea.getText().delete(packet.getPosition() - packet.getLength(), packet.getPosition());
                        } catch(Exception e) {
                            Toast.makeText(Editor.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case 2:
                        //przy insercie analogicznie
                        if(packet.getPacket_msg().length() < 7) return;
                        try {
                            fill_lock = true;
                            textarea.getText().insert(packet.getPosition(), packet.getMsg());
                        } catch(Exception e) {
                            Toast.makeText(Editor.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            }
            else
                Toast.makeText(Editor.this, values[0], Toast.LENGTH_LONG).show();
        }
    }

    //blokujemy możliwość pisania
    public void lock() {
        mKeyListener = textarea.getKeyListener();
        textarea.setKeyListener(null);
    }

    //oblokowujemy możliwość pisania
    public void unlock() {
        textarea.setKeyListener(mKeyListener);
        textarea.setInputType( android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD );
        textarea.setSingleLine(false);
        textarea.setVerticalScrollBarEnabled(true);
        textarea.setHorizontalScrollBarEnabled(false);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        Intent intent = getIntent();
        //pobieramy nazwę pliku, który otowrzyliśmy
        filename = intent.getStringExtra("filename");

        //ustawiamy nazwę jako tytuł Toolbara
        ActionBar bar = getSupportActionBar();
        if (bar != null)
            bar.setTitle(filename);
        else
            Toast.makeText(Editor.this, "No action bar found", Toast.LENGTH_LONG).show();
        //edytujemy właściwości kontrolki
        textarea = (EditText) findViewById(R.id.edittext);
        if (textarea != null) {
            textarea.setInputType( android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD );
            textarea.setSingleLine(false);
            textarea.setVerticalScrollBarEnabled(true);
            textarea.setHorizontalScrollBarEnabled(false);
            textarea.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    //jeśli nie ma blokady, to pobierz zmiany i przetwórz na pakiet
                    if(fill_lock) return;
                    int flag = count >= before ? 2 : 1;
                    int size = count > before ? count : before;
                    if (size == 0) return;
                    String msg = s.toString();
                    TCPpacket pck;
                    if(flag == 1) {
                        if(start < 0){
                            pck = new TCPpacket(flag, size, "del", size);
                        } else
                            pck = new TCPpacket(flag, start+size, "del", size);
                        //Toast.makeText(Editor.this, pck.getPacket_msg(), Toast.LENGTH_LONG).show();
                    }
                    else
                        pck = new TCPpacket(flag, start, msg.substring(start, start+size));
                    //wyślij przygotowany pakiet
                    mTcpClient.sendMessage(pck.getPacket_msg());
                }
            });
            lock();
        } else
            Toast.makeText(Editor.this, "No text area found", Toast.LENGTH_LONG).show();

        //rozpocznij zadanie
        atask = new ConnectTask().execute("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }


    private boolean fetch_lock = false;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(mTcpClient != null) {
            if (id == R.id.action_save) {
                //wysyła informację o odłączeniu od serwera,
                //i zamyka aktywność
                TCPpacket pck = new TCPpacket(5, null, null);
                mTcpClient.sendMessage(pck.getPacket_msg());
                atask.cancel(true);
                mTcpClient.stopClient();
                finish();
            }
            if (id == R.id.action_show && !fetch_lock) {
                //wysyła prośbę o pobranie treści, możliwe tylko raz
                TCPpacket pck = new TCPpacket(8, 0, filename);
                mTcpClient.sendMessage(pck.getPacket_msg());
            }
        }
        else
            Toast.makeText(Editor.this, "tcp is nul", Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }
}
