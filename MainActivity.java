package com.example.root.skeditor;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    //kontrolka listy i adapter elementów
    private ListView list;
    private ArrayAdapter<String> adapter;
    //zadanie asynchorniczne
    private  AsyncTask atask;

    //klient TCP i klasa zadania asynchronicznego
    //z nim powiązanego
    TCPclient mTcpClient;
    public class ConnectTask extends AsyncTask<String, String, TCPclient> {

        @Override
        protected TCPclient doInBackground(String... message) {
            mTcpClient = new TCPclient(new TCPclient.OnMessageReceived() {
                @Override
                public void messageReceived(String message) {
                    publishProgress(message);
                }
            });
            mTcpClient.run();
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            //odebrane wiadomości są dekodowane do obiektu pakietu
            TCPpacket packet = TCPpacket.decode(values[0]);

            if (packet != null) {
                //jeśli dostaliśmy pakiet, utwórz z niego listę plików
                if (packet.getMsg() != null)
                    createFiles(packet.getMsg());
            }
            else
                Toast.makeText(MainActivity.this, values[0], Toast.LENGTH_LONG).show();
        }
    }

    protected void listCreate() {
        list = (ListView) findViewById(R.id.listView);
        if(list == null) return;
        list.setClickable(true);
        //listener obsługujący wybranie elementu z lity
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = (String)list.getAdapter().getItem(position);
                Intent intent = new Intent(MainActivity.this, Editor.class);
                //zatrzymujemy pracę klienta
                mTcpClient.stopClient();
                atask.cancel(true);
                //uruchamiamy nową aktywność
                intent.putExtra("filename", name);
                startActivity(intent);
            }
        });

        //dodajemy header i footer do listy
        LinearLayout listFooterView = (LinearLayout) getLayoutInflater().inflate(R.layout.footer, list, false);
        LinearLayout listHeaderView = (LinearLayout) getLayoutInflater().inflate(R.layout.header, list, false);

        list.addHeaderView(listHeaderView, null, false);
        list.addFooterView(listFooterView, null, false);

        //wypełniamy listę pustą zawartością, aby ją zainicjalizować
        ArrayList<String> carL = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, R.layout.row, carL);

        list.setAdapter(adapter);
    }

    private void fetchForFiles() {
        //jeśli nasz klient jest podłączony, to wyślij zapytanie o listę plików
        if(mTcpClient == null)
            Toast.makeText(MainActivity.this, "tcp is null", Toast.LENGTH_LONG).show();
        else {
            TCPpacket filelist_fetch = new TCPpacket(9, null, null);
            mTcpClient.sendMessage(filelist_fetch.getPacket_msg());
        }
    }

    private void createFiles(String filenames) {
        //dzieli łańcuch znaków na podłańcuchy i wypełnia nimi listę
        ArrayList<String> files = new ArrayList<>(Arrays.asList(filenames.split(",")));
        adapter = new ArrayAdapter<>(this, R.layout.row, files);
        list.setAdapter(adapter);
    }

    public void newFileClick(View v) {
        //fukcja uruchamiana przy kliknięciu footera
        if(mTcpClient == null)
            Toast.makeText(MainActivity.this, "tcp is null", Toast.LENGTH_LONG).show();
        else {
            Intent intent = new Intent(this, Editor.class);
            mTcpClient.stopClient();
            atask.cancel(true);
            intent.putExtra("filename", "newfile");
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fetch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //obsługa ikonek na Toolbarze
        int id = item.getItemId();
        if (id == R.id.action_fetch) {
            fetchForFiles();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //tworzymy listę i rozpoczynami połączenie
        listCreate();
        atask = new ConnectTask().execute("");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTcpClient.stopClient();
        atask.cancel(true);
    }
}