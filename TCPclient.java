package com.example.root.skeditor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class TCPclient {

    public static final String SERVER_IP = "37.247.61.132";
    public static final int SERVER_PORT = 42069;// 8080;
    //wiadomość odbierana przez serwer
    private String mServerMessage;
    //listener dla odbieranych wiadomości
    private OnMessageReceived mMessageListener = null;
    //zmienna kontrolna dla pętli nasłuchującej
    private boolean mRun = false;
    //bufory wejścia i wyjścia
    private PrintWriter mBufferOut;
    private BufferedReader mBufferIn;


    public TCPclient(OnMessageReceived listener) {
        mMessageListener = listener;
    }

    public void sendMessage(String message) {
        if (mBufferOut != null && !mBufferOut.checkError()) {
            mBufferOut.println(message);
            mBufferOut.flush();
        }
    }

    public void stopClient() {
        mRun = false;
        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }
        mMessageListener = null;
        mBufferIn = null;
        mBufferOut = null;
        mServerMessage = null;
    }

    public void run() {
        mRun = true;
        try {
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            if(serverAddr == null)
                return;

            Socket socket = null;

            try {
                socket = new Socket(serverAddr, SERVER_PORT);

                //związanie buforów
                mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                while (mRun) {
                    //odbieranie i wysyłanie wiadomości do przetwarzania
                    mServerMessage = mBufferIn.readLine();
                    if (mServerMessage != null && mMessageListener != null) {
                        mMessageListener.messageReceived(mServerMessage);
                    }

                }
            } catch (Exception e) {
                e.getMessage();
            } finally {
                if(socket != null)
                    socket.close();
            }

        } catch (Exception e) {
            e.getMessage();
        }
    }

    //interface listenera
    public interface OnMessageReceived {
        void messageReceived(String message);
    }

}
